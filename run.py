import sys

from PySide2.QtWidgets import QApplication

from simple_live_qtwidgets.mainwindow import MainWindow


def main():
    a = QApplication(sys.argv)
    w = MainWindow()
    w.show()
    a.exec_()


if __name__ == "__main__":
    main()
