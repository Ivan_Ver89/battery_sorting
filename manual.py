import asyncio
import serial


class ConnectionInterface:
    ser = serial.Serial()
    read_queue = asyncio.Queue()
    send_queue = asyncio.Queue()

    def __init__(self):
        self.ser.port = '/dev/ttyUSB0'
        self.ser.baudrate = 38400

    def connect(self):
        if not self.ser.is_open:
            self.ser.open()
        print(self.ser.is_open)

    async def main(self):
        asyncio.get_event_loop().create_task(self.update())
        while True:
            code = input('> ')
            await self.send_g_code(code)

    async def update(self):
        while self.ser.is_open:
            try:
                msg = self.send_queue.get_nowait()
                self.ser.write(f"{msg}\n".encode('ascii'))
            except asyncio.QueueEmpty:
                pass
            if self.ser.in_waiting >= 1:
                line = self.ser.readline().decode('ascii').replace('\n', '').replace('\r', '')
                if len(line) > 0:
                    await self.read_queue.put(line)
            await asyncio.sleep(1/100)

    async def send_g_code(self, g_code_string=''):
        if self.ser.is_open:
            if len(g_code_string) > 0:
                await self.send_queue.put(f"{g_code_string}\n")
                msg = 'gcode_w8'
                while "OK" not in msg:
                    try:
                        msg = self.read_queue.get_nowait()
                        self.read_queue.task_done()
                    except asyncio.QueueEmpty:
                        pass
                    except Exception:
                        pass
                    await asyncio.sleep(1/10)
                print(f"< {msg}")


if __name__ == '__main__':
    connectionInterface = ConnectionInterface()
    connectionInterface.connect()
    asyncio.run(connectionInterface.main())