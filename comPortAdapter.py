import logging
from threading import Lock

import serial


class Com_port_adapter():
    def __init__(self):
        self.ser = serial.Serial()
        self.lock = Lock()
        self.ser.port = '/dev/ttyUSB0'
        self.ser.baudrate = 38400

    def run(self):
        if not self.ser.is_open:
            self.ser.open()
        logging.warning(f"com port open - {self.ser.is_open}")
        self.__send_string("set spd=100,100,100")

    def send_coordinates(self, obj):
        self.lock.acquire()
        self.__send_string(obj.to_string())
        self.lock.release()

    def __send_string(self, string):
        logging.warning(string)
        self.ser.write(f"{string}\n".encode('ascii'))
        response = ""
        while "OK_DONE" not in response or "OK_SPD" not in response:
            if self.ser.in_waiting >= 1:
                line = self.ser.readline().decode('ascii').replace('\n', '').replace('\r', '')
                if len(line) > 0:
                    response = line
        logging.warning(f"{response}")