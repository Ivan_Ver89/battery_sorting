import base64
import io
import json

import cv2
import numpy as np
import requests
from PIL import Image

from model import update_list, OBJECT_LIST

url = "http://localhost:4000/jsonrpc"
headers = {'content-type': 'application/json'}


mainPoint = 630


def frame_process(converted_ipl_image):
    image_np_array = converted_ipl_image.get_numpy_1D()
    h = converted_ipl_image.Height()
    w = converted_ipl_image.Width()
    frame = np.reshape(image_np_array, (h, w, 4))
    cv2.line(frame, (0, 630), (w, 630), (0, 0, 255), 7)
    cv2.line(frame, (0, 1844), (w, 1844), (0, 0, 255), 7)

    info = gen_neural_network_info(frame)

    update_list([{"cx": i["segmentation_prop"]["center"][0],
                  "cy": i["segmentation_prop"]["center"][1],
                  "w": i["segmentation_prop"]["w"],
                  "h": i["segmentation_prop"]["h"],
                  "ang": i["segmentation_prop"]["ang"],
                  "box_left_down_angle": i["bbox"][1] + i["bbox"][3],
                  "category": i["category_id"]} for i in info])

    for i in info:
        if i["category_id"] == 0:
            cv2.rectangle(frame,
                          (i["bbox"][0], i["bbox"][1]),
                          (i["bbox"][0] + i["bbox"][2], i["bbox"][1] + i["bbox"][3]),
                          (0, 255, 0), 3)
        else:
            cv2.rectangle(frame,
                          (i["bbox"][0], i["bbox"][1]),
                          (i["bbox"][0] + i["bbox"][2],
                           i["bbox"][1] + i["bbox"][3]),
                          (0, 0, 250), 3)

    print(len(OBJECT_LIST), [str(x) for x in OBJECT_LIST])
    return image_np_array


def gen_neural_network_info(frame):
    with io.BytesIO() as output:
        Image.fromarray(frame).convert('RGB').save(output, format="JPEG", quality=50)
        result_image_data = output.getvalue()

    encoded_string = base64.b64encode(result_image_data)

    i = 0
    payload = {
        "method": "predict",
        "params": {"image": encoded_string.decode('utf-8')},
        "jsonrpc": "2.0",
        "id": i,
    }

    response = requests.post(url, data=json.dumps(payload), headers=headers).json()
    return response["result"]["annotations"]