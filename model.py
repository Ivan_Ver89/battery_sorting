from threading import Thread
import time
import math
from comPortAdapter import Com_port_adapter

com_port = Com_port_adapter()
com_port.run()

OBJECT_LIST = set()

radius_mm = 70                   #Расстояние между осями магнита и присосок
one_pix_mm = 0.29                #Масштабный коэффициент
konv_vel_mmsec = 28.092042186    #Скорость движения конвейера

screen_height_px = 1944          #Высота монитора в пикселях
screen_width_px = 2592           #Ширина монитора в пикселях
screen_height_mm = 563.76        #Высота монитора в милиметрах
screen_width_mm = 751.68         #Ширина монитора в милиметрах

left_offset_px = 1326.53
left_offset_mm = 384.69

line_start_det_bufer_px = 630
line_start_det_mm = -1465

line_start_det_bufer_mm = 182.7
line_finish_det_bufer_px = 1844
line_finish_det_bufer_mm = -1139.94


class Battery(Thread):
    #set mag=1 - on magn
    #set s1=1 - on prisoska
    def __init__(self, cx_mm, cy_mm, w, h, alpha, object_front):
        Thread.__init__(self)
        self.cx_mm = cx_mm
        self.cy_mm = cy_mm
        self.w_mm = w
        self.h_mm = w
        self.alpha = alpha
        self.count_condition = 0
        self.category = None
        self.capture_type = None
        self.object_front = object_front
        self.start()

    def run(self):
        while self.cy_mm < 350:
            time.sleep(0.01)
            self.cy_mm += float(konv_vel_mmsec/100)
            self.object_front += float(konv_vel_mmsec/100)
            if self.cy_mm > -380 and not com_port.lock.locked() and self.count_condition < 7:
                self.set_category_and_angle()
                OBJECT_LIST.remove(self)
                com_port.send_coordinates(self)
                break
        else:
            pass
            # сброс не пойманных батареек

    def count(self, arg):
        if arg == 0:
            self.count_condition += 1
        else:
            self.count_condition -= 1

    def set_category_and_angle(self):
        if self.alpha > 180:
            self.alpha -= 180
        if max(self.w_mm, self.h_mm) > 200:
            if max(self.w_mm, self.h_mm) / min(self.w_mm, self.h_mm):
                self.cy_mm -= 45 * math.cos(math.radians(self.alpha))
                self.cy_mm -= 45 * math.sin(math.radians(self.alpha))

    def to_string(self):
        return f"set x={self.cx_mm}\nset y={self.cy_mm - 15}\nset a={0}\nset {self.capture_type}\ngo"

    def __str__(self):
        return f"set x={self.cx_mm}\nset y={self.cy_mm - 15}\nset a={0}\nset {self.capture_type}\ngo"


def update_list(objects_rect):
    for rect in objects_rect:
        if len(OBJECT_LIST) == 0:
            if (rect["cy"] > line_start_det_bufer_px):
                OBJECT_LIST.add(Battery(cx_mm=convert_to_mm(x_px=rect["cx"]), cy_mm=convert_to_mm(y_px=rect["cy"]),
                                        h=min(rect["h"], rect["w"]) * one_pix_mm,
                                        w=max(rect["h"], rect["w"]) * one_pix_mm, alpha=0,
                                        object_front=convert_to_mm(y_px=rect["box_left_down_angle"])))
        else:
            is_new_object = False
            for obj in OBJECT_LIST:
                if math.hypot(convert_to_mm(x_px=rect["cx"]) - obj.cx_mm,
                              convert_to_mm(x_px=rect["cy"]) - obj.cy_mm) > 50:
                    is_new_object = True
                else:
                    is_new_object = False
                    if rect["box_left_down_angle"] < line_finish_det_bufer_px:
                        obj.count(rect["category"])
                    break
            if is_new_object:
                if (rect["cy"] > line_start_det_bufer_px and rect["cy"] < line_finish_det_bufer_px):
                    OBJECT_LIST.add(Battery(cx_mm=convert_to_mm(x_px=rect["cx"]), cy_mm=convert_to_mm(y_px=rect["cy"]),
                                            h=min(rect["h"], rect["w"]) * one_pix_mm,
                                            w=max(rect["h"], rect["w"]) * one_pix_mm, alpha=0,
                                            object_front=convert_to_mm(y_px=rect["box_left_down_angle"])))


def convert_to_mm(x_px=0, y_px=0):
    if x_px != 0:
        return left_offset_mm - x_px * one_pix_mm
    if y_px != 0:
        return line_start_det_mm - line_start_det_bufer_mm + y_px * one_pix_mm